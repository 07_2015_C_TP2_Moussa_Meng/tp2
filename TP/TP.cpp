﻿#include <iostream>
#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include "RockUtiles.h"
using namespace std;

// Declaration des functions de premier partie
void afficherTerrain(int nbLignes, int nbColonnes);
int recupererTouche();
int calculerDirectionTouche(int touche);
void positionAleatoire(int nbLignes, int nbColonnes, int &posX, int &posY);
void deplacerSerpentI(int direction, int &posX, int &posY);

// Declaration des functions de deuxieme partie
int saisirNiveau();
void creerSouris(int nbLignes, int nbColonnes, int sourisX[], int sourisY[], int nbsouris);
void afficherSouris(int sourisX[], int sourisY[], int nbSouris);
void deplacerSerpentII(int direction, int serpentX[], int serpentY[],
	int &tailleSerpent, int sourisX[], int sourisY[], int &nbSouris);

// Declaration des functions de  troixieme partie
void deplacerSouris(int sourisX[], int sourisY[], int nbSouris, int compteur, int teteX, int teteY);

// Declaration d'autres functions
void attendre(int millisecond);
int grandAbs(int nTete, int nSouris);
void afficherSucces();
void afficherCognerCadre();
void afficherEatSelf();

// Declaration des constants
const int NB_LIGNES = 30, NB_COLONNES = 70;
const int CD = 5, LD = 2;  // Definition du CADRE du jeu :   ( CD : X de gauche en haut;     LD : Y de gauche en haut )

int main()
{
	srand((unsigned int)time(NULL));
	int direction = -1;
	int sourisX[20], sourisY[20];
	int serpentX[21], serpentY[21];
	int tailleSerpent = 1;

	int nbSouris = saisirNiveau();
	system("CLS");  // Effacer les traces de la saisie de niveau
	creerSouris(NB_LIGNES, NB_COLONNES, sourisX, sourisY, nbSouris);
	afficherSouris(sourisX, sourisY, nbSouris);
	afficherTerrain(NB_LIGNES, NB_COLONNES);
	positionAleatoire(NB_LIGNES, NB_COLONNES, serpentX[0], serpentY[0]);

	// Affichier le nombre des souris
	gotoXY(32, 1);
	color(FOREGROUND_GREEN + FOREGROUND_RED + FOREGROUND_INTENSITY);
	cout << "Souris reste : ";
	gotoXY(47, 1);
	color(FOREGROUND_RED + FOREGROUND_INTENSITY);
	cout << nbSouris;
	color(FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);

	// Attendre le depart
	while (direction == -1)
	{
		gotoXY(serpentX[0], serpentY[0]);
		cout << "X";
		attendre(300);
		gotoXY(serpentX[0], serpentY[0]);
		cout << " ";
		attendre(300);
		// Recuperer la valeur de touche, la transferer en diretion (0, 1, 2, 3)
		direction = calculerDirectionTouche(recupererTouche());
	}

	/*	deplacerSerpentI(direction, serpentX[0], serpentY[0]); */

	deplacerSerpentII(direction, serpentX, serpentY, tailleSerpent, sourisX, sourisY, nbSouris);
	while (1) {};
	return 0;
}

// Definition des functions de premier partie
void afficherTerrain(int nbLignes, int nbColonnes)
{
	/*
	Tâche : afficher le contour du terrain
	Paramètres: le nombre de lignes et de colonnes du terrain rectangulaire
	*/
	setDimensionFenetre(0, 0, nbColonnes + 2 * CD, nbLignes + 2 * LD);     // Dimention de Fenetre
	cadre(CD, LD, CD + nbColonnes, LD + nbLignes, 0x0004);                      // Dimention de Cadre
}

int recupererTouche()
{
	/*
	Tâche : tester si le joueur a appuyé sur une  touche
	Retour : retourner le code ASCII de la touche pressée, -1 sinon
	*/
	int touche = _kbhit() ? _getch() : -1;
	return  touche;
}

int calculerDirectionTouche(int touche)
{
	/*
	Tâche: calculer la direction correspondant à une touche ou -1
	Paramètre: le code d'une touche (w, a, s ou d)
	Retour: la direction qui correspond à la touche
	(0: droite, 1: gauche, 2: haut, 3: bas)
	*/
	int direction;
	switch (touche)
	{
	case 100:	direction = 0;
		break;
	case 97: direction = 1;
		break;
	case 119: direction = 2;
		break;
	case 115:	direction = 3;
		break;
	default: direction = -1;
		break;
	}
	return direction;
}

void positionAleatoire(int nbLignes, int nbColonnes, int &posX, int &posY)
{
	/*
	Tâche: calculer une position aléatoire sur le terrain
	Paramètres: les dimensions du terrain en entrée ((int nbLignes, int nbColonnes)
	les coordonnées de la position aléatoire en sortie (int &posX, int &posY)
	*/
	int x, y;
	do
	{
		x = rand() % (nbColonnes - 1) + CD + 1;
		y = rand() % (nbLignes - 1) + LD + 1;
	} while (getCharXY(x, y) != ' ');
	posX = rand() % (nbColonnes - 1) + CD + 1;
	posY = rand() % (nbLignes - 1) + LD + 1;
}

void deplacerSerpentI(int direction, int &posX, int &posY)
{
	/*
	Tâche: déplacer le serpent d'une seule case dans la direction donnée. Le serpent est à l'écran avant l'appel et au retour de la fonction
	Paramètres: la direction du serpent en entrée, et la position du serpent en entrée / sortie
	*/
	while (1)
	{
		while ((direction == 1 || direction == -1) && posX > CD)
		{
			gotoXY(posX, posY);
			if (posX % 2 == 0)
				cout << "-";
			else
				cout << ">";
			attendre(300);
			gotoXY(posX, posY);
			cout << " ";
			posX--;
			direction = calculerDirectionTouche(recupererTouche());  // Transferer la valeur de touche en diretion (0, 1, 2, 3)
			if (direction == 0)
				direction = -1;
		}
		if (posX == CD)
			//fin = true;
			//break;
			return;

		while ((direction == 0 || direction == -1) && posX < CD + NB_COLONNES)
		{
			gotoXY(posX, posY);
			if (posX % 2 == 0)
				cout << "-";
			else
				cout << "<";
			attendre(300);
			gotoXY(posX, posY);
			cout << " ";
			posX++;
			direction = calculerDirectionTouche(recupererTouche());  // Transferer la valeur de touche en diretion (0, 1, 2, 3)
			if (direction == 1)
				direction = -1;
		}
		if (posX == CD + NB_COLONNES)
			//fin = true;
			//break;
			return;

		while ((direction == 2 || direction == -1) && posY > LD)
		{
			gotoXY(posX, posY);
			if (posY % 2 == 0)
				cout << "|";
			else
				cout << "V";
			attendre(300);
			gotoXY(posX, posY);
			cout << " ";
			posY--;
			direction = calculerDirectionTouche(recupererTouche());  // Transferer la valeur de touche en diretion (0, 1, 2, 3)
			if (direction == 3)
				direction = -1;
		}
		if (posY == LD)
			//fin = true;
			//break;
			return;

		while ((direction == 3 || direction == -1) && posY < LD + NB_LIGNES)
		{
			gotoXY(posX, posY);
			if (posY % 2 == 0)
				cout << "|";
			else
				cout << "^";
			attendre(300);
			gotoXY(posX, posY);
			cout << " ";
			posY++;
			direction = calculerDirectionTouche(recupererTouche());  // Transferer la valeur de touche en diretion (0, 1, 2, 3)
			if (direction == 2)
				direction = -1;
		}
		if (posY == LD + NB_LIGNES)
			//fin = true;
			//break;
			return;
	}
}

// Definition des functions de deuxieme partie
int saisirNiveau()
{
	/*
	Tâche: lire le niveau de difficulté avec tests de validation d'entrée
	Retour: le niveau (= le nombre de souris initialement sur le terrain)
	*/
	int nb;
	bool fin = false;
	do
	{
		cout << "Entrer le nombre de souris (1 - 20) : ";
		cin >> nb;

		if (cin.peek() != '\n' || nb<1 || nb >20)
		{
			cin.clear();
			cin.ignore(512, '\n');
			cout << "Erreur! Refaite le !" << endl;
		}
		else
			fin = true;
	} while (!fin);
	return nb;
}

void creerSouris(int nbLignes, int nbColonnes, int sourisX[], int sourisY[], int nbsouris)
{
	/*
	Tâche: générer les nbSouris aléatoirement sur le terrain
	Paramètres: les dimensions du terrain (int nbLignes, int nbColonnes),
	les tableaux de coordonnées (int sourisX[], int sourisY[]),
	le nombre de souris (int nbsouris)
	*/
	int x, y;
	for (int i = 0; i < nbsouris; i++)
	{
		do
		{
			x = rand() % (nbColonnes - 1) + CD + 1;
			y = rand() % (nbLignes - 1) + LD + 1;
		} while (getCharXY(x, y) != ' ');
		sourisX[i] = x;
		sourisY[i] = y;
	}
}

void afficherSouris(int sourisX[], int sourisY[], int nbSouris)
{
	/*
	Tâche: afficher les souris
	Paramètres: les tableaux de coordonnées et le nombre de souris
	*/
	for (int i = 0; i < nbSouris; i++)
	{
		gotoXY(sourisX[i], sourisY[i]);
		color(FOREGROUND_GREEN + FOREGROUND_INTENSITY);
		cout << 'O';

	}
}

void deplacerSerpentII(int direction, int serpentX[], int serpentY[],
	int &tailleSerpent, int sourisX[], int sourisY[], int &nbSouris)
{
	/*
	Tâche:
	déplacer le serpent d'une seule case dans la direction donnée.
	Le serpent est à l'écran avant l'appel et au retour de la fonction
	Paramètres:
	en entrée : la direction du serpent (int direction),
	en entrée/sortie : les tableaux de coordonnées du serpent (int serpentX[], int serpentY[]),
	la taille du serpent (int &tailleSerpent),
	les tableaux de coordonnées des souris (int sourisX[], int sourisY[]),
	le nombre de souris (int &nbSouris)
	*/
	int tempQueueX, tempQueueY;
	unsigned short int cpt = 1;
	while (1)
	{
		// Direction ves la gauche
		while (direction == 1 || direction == -1)
		{
			// D'abord enlever la queue
			gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
			cout << " ";

			// Sauvegarder la position de la queue au dernier moment
			tempQueueX = serpentX[tailleSerpent - 1];
			tempQueueY = serpentY[tailleSerpent - 1];

			// Definition du position du corps du serpent. A partir de deuxieme case.  Decroissant
			for (int m = tailleSerpent - 1; m > 0; m--)
			{
				serpentX[m] = serpentX[m - 1];
				serpentY[m] = serpentY[m - 1];
			}

			// Definition du position la tete (Fait apres la definition du corps)
			serpentX[0]--;

			// Echec du jeu a cause de manger soi-meme
			if (getCharXY(serpentX[0], serpentY[0]) == 'X')
			{
				afficherEatSelf();
				return;
			}

			// Detecter souris && Afficher la queue si le serpent est prolonger
			int i = 0;
			while (i < nbSouris && (sourisX[i] != serpentX[0] || sourisY[i] != serpentY[0]))
				i++;
			if (i < nbSouris)
			{
				tailleSerpent++;
				serpentX[tailleSerpent - 1] = tempQueueX;
				serpentY[tailleSerpent - 1] = tempQueueY;
				sourisX[i] = sourisX[nbSouris - 1];
				sourisY[i] = sourisY[nbSouris - 1];
				nbSouris--;
				gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
				cout << "X";
			}

			// Afficher le nombre de souris qui reste
			gotoXY(47, 1);
			cout << "   ";
			gotoXY(47, 1);
			color(FOREGROUND_RED + FOREGROUND_INTENSITY);
			cout << nbSouris;
			color(FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);

			// affichage de la tete et le deuxieme node du serpent
			gotoXY(serpentX[0], serpentY[0]);
			if (cpt % 2 == 0)
			{ cout << "-"; }
			else { cout << ">"; }
			if (tailleSerpent > 1)
			{
				gotoXY(serpentX[1], serpentY[1]);
				cout << "X";
			}

			attendre(300);

			// Detecter le clavier
			direction = calculerDirectionTouche(recupererTouche());
			if (direction == 0)
				direction = -1;

			cpt++;

			deplacerSouris(sourisX, sourisY, nbSouris, cpt, serpentX[0], serpentY[0]);

			// Fin de jeu (apres la function deplacerSouris() pour qu'il y a pas de souris qui reste dans l'ecran)
			if (nbSouris == 0)
			{
				afficherSucces();
				return;
			}

			// Echec du jeu a cause de deborder
			if (serpentX[0] == CD)
			{
				afficherCognerCadre();
				return;
			}
		}

		// Direction vers la droite
		while (direction == 0 || direction == -1)
		{
			// D'abord enlever la queue
			gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
			cout << " ";

			// Sauvegarder la position de la queue au dernier moment
			tempQueueX = serpentX[tailleSerpent - 1];
			tempQueueY = serpentY[tailleSerpent - 1];

			// Definition du position du corps du serpent. A partir de deuxieme case.  Decroissant
			for (int m = tailleSerpent - 1; m > 0; m--)
			{
				serpentX[m] = serpentX[m - 1];
				serpentY[m] = serpentY[m - 1];
			}
			// Definition du position la tete (fait apres la definition du corps)
			serpentX[0]++;

			// Echec du jeu a cause de manger soi-meme
			if (getCharXY(serpentX[0], serpentY[0]) == 'X')
			{
				afficherEatSelf();
				return;
			}

			// Detecter souris && Afficher la queue si le serpent est prolonger
			int i = 0;
			while (i < nbSouris && (sourisX[i] != serpentX[0] || sourisY[i] != serpentY[0]))
				i++;
			if (i < nbSouris)
			{
				tailleSerpent++;
				serpentX[tailleSerpent - 1] = tempQueueX;
				serpentY[tailleSerpent - 1] = tempQueueY;
				sourisX[i] = sourisX[nbSouris - 1];
				sourisY[i] = sourisY[nbSouris - 1];
				nbSouris--;
				gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
				cout << "X";
			}

			// Afficher le nombre de souris qui reste
			gotoXY(47, 1);
			cout << "   ";
			gotoXY(47, 1);
			color(FOREGROUND_RED + FOREGROUND_INTENSITY);
			cout << nbSouris;
			color(FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);

			// affichage de la tete et deuxieme node du serpent
			gotoXY(serpentX[0], serpentY[0]);
			if (cpt % 2 == 0)
			{ cout << "-"; }
			else { cout << "<"; }
			if (tailleSerpent > 1)
			{
				gotoXY(serpentX[1], serpentY[1]);
				cout << "X";
			}

			attendre(300);

			direction = calculerDirectionTouche(recupererTouche());
			if (direction == 1)
				direction = -1;
			deplacerSouris(sourisX, sourisY, nbSouris, cpt, serpentX[0], serpentY[0]);
			cpt++;

			// Fin de jeu (apres la function deplacerSouris() pour qu'il y a pas de souris qui reste dans l'ecran)
			if (nbSouris == 0)
			{
				afficherSucces();
				return;
			}

			// Echec du jeu a cause de deborder
			if (serpentX[0] == CD + NB_COLONNES)
			{
				afficherCognerCadre();
				return;
			}
		}

		// Direction en haut
		while (direction == 2 || direction == -1)
		{
			// D'abord enlever la queue
			gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
			cout << " ";

			// Sauvegarder la position de la queue au dernier moment
			tempQueueX = serpentX[tailleSerpent - 1];
			tempQueueY = serpentY[tailleSerpent - 1];

			// Definition du position du corps du serpent. A partir de deuxieme case.  Decroissant
			for (int m = tailleSerpent - 1; m > 0; m--)
			{
				serpentX[m] = serpentX[m - 1];
				serpentY[m] = serpentY[m - 1];
			}
			// Definition du position la tete (fait apres la definition du corps)
			serpentY[0]--;

			// Echec du jeu a cause de manger soi-meme
			if (getCharXY(serpentX[0], serpentY[0]) == 'X')
			{
				afficherEatSelf();
				return;
			}

			// Detecter souris && Afficher la queue si le serpent est prolonger
			int i = 0;
			while (i < nbSouris && (sourisX[i] != serpentX[0] || sourisY[i] != serpentY[0]))
				i++;
			if (i < nbSouris)
			{
				tailleSerpent++;
				serpentX[tailleSerpent - 1] = tempQueueX;
				serpentY[tailleSerpent - 1] = tempQueueY;
				sourisX[i] = sourisX[nbSouris - 1];
				sourisY[i] = sourisY[nbSouris - 1];
				nbSouris--;
				gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
				cout << "X";
			}

			// Afficher le nombre de souris qui reste
			gotoXY(47, 1);
			cout << "   ";
			gotoXY(47, 1);
			color(FOREGROUND_RED + FOREGROUND_INTENSITY);
			cout << nbSouris;
			color(FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);

			// affichage de la tete et deuxieme node du serpent
			gotoXY(serpentX[0], serpentY[0]);
			if (cpt % 2 == 0)
			{ cout << "|"; }
			else { cout << "v"; }
			if (tailleSerpent > 1)
			{
				gotoXY(serpentX[1], serpentY[1]);
				cout << "X";
			}

			attendre(300);

			direction = calculerDirectionTouche(recupererTouche());
			if (direction == 3)
				direction = -1;
			deplacerSouris(sourisX, sourisY, nbSouris, cpt, serpentX[0], serpentY[0]);
			cpt++;

			// Fin de jeu (apres la function deplacerSouris() pour qu'il y a pas de souris qui reste dans l'ecran)
			if (nbSouris == 0)
			{
				afficherSucces();
				return;
			}

			// Echec du jeu a cause de deborder
			if (serpentY[0] == LD)
			{
				afficherCognerCadre();
				return;
			}
		}

		// Direction en bas
		while (direction == 3 || direction == -1)
		{
			// D'abord enlever la queue
			gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
			cout << " ";

			// Sauvegarder la position de la queue au dernier moment
			tempQueueX = serpentX[tailleSerpent - 1];
			tempQueueY = serpentY[tailleSerpent - 1];

			// Definition du position du corps du serpent. A partir de deuxieme case.  Decroissant
			for (int m = tailleSerpent - 1; m > 0; m--)
			{
				serpentX[m] = serpentX[m - 1];
				serpentY[m] = serpentY[m - 1];
			}
			// Definition du position la tete (fait apres la definition du corps)
			serpentY[0]++;

			// Echec du jeu a cause de manger soi-meme
			if (getCharXY(serpentX[0], serpentY[0]) == 'X')
			{
				afficherEatSelf();
				return;
			}

			// Detecter souris && Afficher la queue si le serpent est prolonger
			int i = 0;
			while (i < nbSouris && (sourisX[i] != serpentX[0] || sourisY[i] != serpentY[0]))
				i++;
			if (i < nbSouris)
			{
				tailleSerpent++;
				serpentX[tailleSerpent - 1] = tempQueueX;
				serpentY[tailleSerpent - 1] = tempQueueY;
				sourisX[i] = sourisX[nbSouris - 1];
				sourisY[i] = sourisY[nbSouris - 1];
				nbSouris--;
				gotoXY(serpentX[tailleSerpent - 1], serpentY[tailleSerpent - 1]);
				cout << "X";
			}

			// Afficher le nombre de souris qui reste
			gotoXY(47, 1);
			cout << "   ";
			gotoXY(47, 1);
			color(FOREGROUND_RED + FOREGROUND_INTENSITY);
			cout << nbSouris;
			color(FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);

			// affichage de la tete et deuxieme node du serpent
			gotoXY(serpentX[0], serpentY[0]);
			if (cpt % 2 == 0)
			{ cout << "|"; }
			else { cout << "^"; }
			if (tailleSerpent > 1)
			{
				gotoXY(serpentX[1], serpentY[1]);
				cout << "X";
			}

			attendre(300);

			direction = calculerDirectionTouche(recupererTouche());
			if (direction == 2)
				direction = -1;

			deplacerSouris(sourisX, sourisY, nbSouris, cpt, serpentX[0], serpentY[0]);

			cpt++;

			// Fin de jeu (apres la function deplacerSouris() pour qu'il y a pas de souris qui reste dans l'ecran)
			if (nbSouris == 0)
			{
				afficherSucces();
				return;
			}

			// Echec du jeu a cause de deborder
			if (serpentY[0] == LD + NB_LIGNES)
			{
				afficherCognerCadre();
				return;
			}
		}
	}
}

void deplacerSouris(int sourisX[], int sourisY[], int nbSouris, int compteur, int teteX, int teteY)
{
	/*
	Tâche: déplacer les souris (aléatoirement ou intelligemment)
	Paramètres: les tableaux de coordonnées (int sourisX[], int sourisY[]),
	le nombre de souris (int nbSouris),
	le compteur de bouicle (int compteur),
	la position de la tête du serpent (int teteX, int teteY)
	*/
	int directionSouris;

	if (compteur % 2 == 0)  // pour que la vitesse des souris soit la moitie du serpent
	{
		for (int i = 0; i < nbSouris; i++)
		{
			// si la distance entre souris et serpent est moins de 10
			if (abs(sourisX[i] - teteX) <= 15 && abs(sourisY[i] - teteY) <= 15)
			{
				gotoXY(sourisX[i], sourisY[i]);  // position precedente
				cout << " ";

				int tb[4] = { 0, 1, 2, 3 };  // 4 possibilite de direction
				int n = 4;  // nombre d'elements du tableau

				if (getCharXY(sourisX[i] + 1, sourisY[i]) != ' ')
				{
					tb[0] = -1;  // enlever la possibilite de droite
					n--;
				}
				if (getCharXY(sourisX[i] - 1, sourisY[i]) != ' ')
				{
					tb[1] = -1;  // enlever la possibilite de droite
					n--;
				}
				if (getCharXY(sourisX[i], sourisY[i] + 1) != ' ')
				{
					tb[3] = -1; // enlever la possibilite de bas
					n--;
				}
				if (getCharXY(sourisX[i], sourisY[i] - 1) != ' ')
				{
					tb[2] = -1;  // enlever la possibilite de haut
					n--;
				}

				if (n > 0 && n < 4) // si a cote de souris, il y a un ou plusieurs obstacles mais pas 4 obstacles
				{   // les souris se deplacent aleatoirement
					int indice = rand() % n;
					int m = -1;  // compteur, pour trouver l'element positif selon indice aleatoire
					int I = 0;  // indice du nouveau tableau dont la taille est reduite
					while (m != indice)
					{
						if (tb[I] >= 0)
							m++;
						I++;
					}
					switch (tb[I - 1])
					{
					case 0: sourisX[i] ++; break;
					case 1: sourisX[i] --; break;
					case 2: sourisY[i] --; break;
					case 3: sourisY[i] ++; break;
					}
				}
				else if (n == 4)  // si a cote de souris, il n'y a aucun obstacle (si n==0, la souris bouge pas)
				{   // les souris se deplacent pour s'eloigner du serpent
					directionSouris = rand() % 2;
					if (directionSouris == 0)
					{ sourisX[i] = grandAbs(teteX, sourisX[i]); }
					else
					{ sourisY[i] = grandAbs(teteY, sourisY[i]); }
				}

				gotoXY(sourisX[i], sourisY[i]);  // position suivante
				color(FOREGROUND_GREEN + FOREGROUND_INTENSITY);
				cout << "O";
				color(FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);
			}
		}
	}
}

void attendre(int millisecond)
{
	/*
	Tâche: faire un délay entre 2 actions
	Paramètres: milliseconde
	*/
	int debut = clock();
	while (clock() < debut + millisecond) {};
}

int grandAbs(int nTete, int nSouris)
{ 	/*
	Tâche: calculer la position de souris la plus loin du serpent
	Paramètres: X / Y de la tête du serpent (int nTete),
	le nombre de souris (int nSouris)
	*/
	return abs(nSouris + 1 - nTete) >= abs(nSouris - 1 - nTete) ? nSouris + 1 : nSouris - 1;
}

void afficherSucces()
{
	/*
	Tâche: afficher les félicitations lors que l'utilisateur a réussi le jeu
	*/
	gotoXY(30, 15);
	color(BACKGROUND_RED + FOREGROUND_GREEN + FOREGROUND_BLUE + FOREGROUND_RED + FOREGROUND_INTENSITY);
	cout << "F E L I C I T A T I O N  !";
	color(FOREGROUND_INTENSITY);
	gotoXY(25, 33);
}

void afficherCognerCadre()
{
	/*
	Tâche: prévenir de la fin du jeu lors que le serpent a mangé son propre corps
	*/
	gotoXY(27, 15);
	color(BACKGROUND_GREEN + BACKGROUND_RED + BACKGROUND_INTENSITY);
	cout << "INTERDIT DE SE COGNER LA TETE !" << endl;
	color(FOREGROUND_INTENSITY);
	gotoXY(25, 33);
}

void afficherEatSelf()
{
	/*
	Tâche: prévenir de la fin du jeu lors que le serpent a heurté le cadre
	*/
	gotoXY(27, 15);
	color(BACKGROUND_GREEN + BACKGROUND_RED + BACKGROUND_INTENSITY);
	cout << "INTERDIT DE MANGER SOI MEME !" << endl;
	color(FOREGROUND_INTENSITY);
	gotoXY(25, 33);
}